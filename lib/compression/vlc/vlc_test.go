package vlc

import (
	// "reflect"
	"reflect"
	"testing"
)

func Test_prepareText(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Basic test",
			args: args{
				s: "Hello Rouse",
			},
			want: "!hello !rouse",
		},
		{
			name: "Empty string",
			args: args{
				s: "",
			},
			want: "",
		},
		{
			name: "String with exclamation",
			args: args{
				s: "Hello Rouse!!",
			},
			want: "!hello !rouse!!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := prepareText(tt.args.s); got != tt.want {
				t.Errorf("prepareText() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_encodeBinary(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{s: "!abc", name: "Basic Test", want: "0010000110000010000101"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := encodeBinary(tt.s); got != tt.want {
				t.Errorf("encodeBinary() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEncode(t *testing.T) {
	tests := []struct {
		name string
		str  string
		want []byte
	}{
		{
			name: "Basic Test #1",
			str:  "My name is Ted",
			want: []byte{32, 48, 60, 24, 119, 74, 228, 77, 40},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			encoder := New()
			if got := encoder.Encode(tt.str); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Encode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecode(t *testing.T) {
	tests := []struct {
		name string
		data []byte
		want string 
	}{
		{name: "Basic Test #1", data: []byte{32, 48, 60, 24, 119, 74, 228, 77, 40}, want: "My name is Ted"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			decoder := New()
			if got := decoder.Decode(tt.data); got != tt.want {
				t.Errorf("Decode() = %v, want %v", got, tt.want)
			}
		})
	}
}
