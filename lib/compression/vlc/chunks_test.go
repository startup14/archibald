package vlc

import (
	"reflect"
	"testing"
)

func Test_splitByChunks(t *testing.T) {
	type args struct {
		bStr      string
		chunkSize int
	}
	tests := []struct {
		name string
		args args
		want BinaryChunks
	}{
		{
			name: "Basic Test",
			args: args{bStr: "0010000110000010000101", chunkSize: CHUNK_SIZE},
			want: BinaryChunks{"00100001", "10000010", "00010100"},
		}, // TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := splitByChunks(tt.args.bStr, tt.args.chunkSize); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("splitByChunks() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBinaryChunks_Join(t *testing.T) {
	tests := []struct {
		name string
		bcs  BinaryChunks
		want string
	}{
		{name: "Basic Test #1", bcs: BinaryChunks{"10000000", "00101111"}, want: "1000000000101111"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.bcs.Join(); got != tt.want {
				t.Errorf("BinaryChunks.Join() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewBinaryChunks(t *testing.T) {
	tests := []struct {
		name string
		data []byte
		want BinaryChunks
	}{
		{name: "Basic Test #1", data: []byte{20, 30, 60, 18}, want: BinaryChunks{"00010100", "00011110", "00111100", "00010010"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewBinaryChunks(tt.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewBinaryChunks() = %v, want %v", got, tt.want)
			}
		})
	}
}
