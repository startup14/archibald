package vlc

import (
	"fmt"
	"strconv"
	"strings"
	"unicode/utf8"
)

type BinaryChunk string
type BinaryChunks []BinaryChunk

const (
	CHUNK_SIZE = 8
)

// Joins binary chunks in whole string and returns it
func (bcs BinaryChunks) Join() string {
	var buf strings.Builder
	for _, bc := range bcs {
		buf.WriteString(string(bc))
	}
	return buf.String()
}

func (bcs BinaryChunks) Bytes() []byte {
	res := make([]byte, 0, len(bcs))
	for _, bc := range bcs {
		res = append(res, bc.Byte())
	}
	return res
}

func (bc BinaryChunk) Byte() byte {
	num, err := strconv.ParseUint(string(bc), 2, CHUNK_SIZE)
	if err != nil {
		panic("Can't parse binary chunk: " + err.Error())
	}

	return byte(num)
}


func NewBinaryChunks(data []byte) BinaryChunks {
	res := make(BinaryChunks, 0, len(data))
	for _, code := range data {
		res = append(res, NewBinaryChunk(code))
	}
	return res
}

func NewBinaryChunk(b byte) BinaryChunk {
	return BinaryChunk(fmt.Sprintf("%08b", b))
}

func splitByChunks(bStr string, chunkSize int) BinaryChunks {
	strLen := utf8.RuneCountInString(bStr)

	chunksCount := strLen / chunkSize
	if strLen % chunkSize != 0 {
		chunksCount++
	}

	res := make(BinaryChunks, 0, chunksCount)

	var buf strings.Builder

	for i, ch := range bStr {
		buf.WriteString(string(ch))
		if (i+1) % chunkSize == 0 {
			res = append(res, BinaryChunk(buf.String()))
			buf.Reset()
		}
	}

	if buf.Len() > 0 {
		lastChunk := buf.String()
		lastChunk += strings.Repeat("0", chunkSize % len(lastChunk))
		res = append(res, BinaryChunk(lastChunk))
	}

	return res
}

