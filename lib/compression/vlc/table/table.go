package table

import "strings"

type Generator interface {
	NewTable(text string) EncodingTable
}

type decodingNode struct {
	Value string
	ZeroLeft *decodingNode
	OneRight *decodingNode
}

type EncodingTable map[rune]string

func (rootNode *decodingNode) Decode(s string) string {

	var buf strings.Builder

	currNode := rootNode

	for _, ch := range s {
		if currNode.Value != "" {
			buf.WriteString(currNode.Value)
			currNode = rootNode
		}

		switch ch {
		case '0':
			currNode = currNode.ZeroLeft
		case '1':
			currNode = currNode.OneRight
		}
	}
	
	if currNode.Value != "" {
		buf.WriteString(currNode.Value)
	}

	return buf.String()
}

// node is ref because we must change node tree itself, not struct
func (node *decodingNode) add(code string, value string) {
	// 00010(0) <- value

	currNode := node

	for _, ch := range code {
		switch ch {
		case '0':
			if currNode.ZeroLeft == nil {
				currNode.ZeroLeft  = &decodingNode{}
			}
			currNode = currNode.ZeroLeft
		case '1':
			if currNode.OneRight == nil {
				currNode.OneRight  = &decodingNode{}
			}
			currNode = currNode.OneRight
		}
	}

	currNode.Value = string(value)
}

func (table EncodingTable) DecodingNodes() decodingNode {
	res := decodingNode{}

	for ch, code := range table {
		res.add(code, string(ch))
	}

	return res
}