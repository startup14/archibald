package shannon_fano

import (
	"archibald/lib/compression/vlc/table"
	"fmt"
	"strings"

	// "fmt"
	"sort"
)

type Generator struct {}

type charStat map[rune]int

type encodingTable map[rune]code

type code struct {
	Char rune
	Quantity int
	Bits int32
	Size int
}

func NewGenerator() Generator {
	return Generator{}
}

func (g Generator) NewTable(text string) table.EncodingTable {
	stat := newCharStat(text)
	table := build(stat)
	return table.Export() 
}

func (table encodingTable) Export() map[rune]string {
	res := make(map[rune]string)

	for k, v := range table {
		byteString := fmt.Sprintf("%b", v.Bits)	
		if lenDiff := v.Size - len(byteString); lenDiff > 0 {
			byteString = strings.Repeat("0", lenDiff) + byteString
		}
		res[k] = byteString
	}

	return res
}

func build(stat charStat) encodingTable {
	codes := make([]code, 0, len(stat))

	for ch, qty := range stat {
		codes = append(codes, code{Char: ch, Quantity: qty})	
	}

	sort.Slice(codes, func(i, j int) bool {
		if codes[i].Quantity != codes[j].Quantity {
			return codes[i].Quantity > codes[j].Quantity
		}
		return codes[i].Char < codes[j].Char
	})

	assignCodes(codes)

	res := make(encodingTable)

	for _, code := range codes {
		res[code.Char] = code	
	}
	return res
}

func assignCodes(codes []code) {
	// fix case with one code
	if len(codes) < 2 {
		return
	}

	// devide codes on parity quantity
	divider := bestDividerPosition(codes)

	// add 0 or 1
	for i := 0; i < len(codes); i++ {
		codes[i].Bits <<= 1	
		codes[i].Size++
		if i >= divider {
			codes[i].Bits |= 1
		}
	}

	assignCodes(codes[:divider])
	assignCodes(codes[divider:])
}

func bestDividerPosition(codes []code) int {
	total := 0
	for _, code := range codes {
		total += code.Quantity
	}

	left := 0 
	prevDiff := total // or math.MaxInt ?
	bestPosition := 0

	for i :=0; i < len(codes); i++ {
		left += codes[0].Quantity
		right := total - left
		diff := abs(right - left)
		if diff >= prevDiff {
			break
		}
		prevDiff = diff
		bestPosition = i + 1
	}

	return bestPosition
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func newCharStat(text string) charStat  {
	res := make(charStat, )

	for _, ch := range text {
		res[ch]++
	}

	return res
}