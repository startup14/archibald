package vlc

import (
	"archibald/lib/compression/vlc/table"
	"strings"
	"unicode"
)

type EncoderDecoder struct {}

func New() EncoderDecoder {
	return EncoderDecoder{}
}

func (EncoderDecoder) Encode(s string) []byte {
	// prepare text: B -> !b
	s = prepareText(s)
	// encode to binary: text -> 0101010111
	binS := encodeBinary(s)
	// split binary to chunks(8): bits to bytes -> '01110000 00001111'
	chunks := splitByChunks(binS, CHUNK_SIZE)
	// bytes to hex: -> '20 44 FF'
	// return hex
	return chunks.Bytes() 
}

func (EncoderDecoder) Decode(encoded []byte) (string) {

	binString := NewBinaryChunks(encoded).Join() 

	// 3. build decoding bin tree
	decodingTree := getEncodingTable().DecodingNodes()

	// 4. build string from tree -> string
	return exportText(decodingTree.Decode(binString)) // My name is Ted -> !my name is !ted
}

// encodes s into binary codes string without spaces
func encodeBinary(s string) string {
	var buf strings.Builder

	for _, ch := range s {
		buf.WriteString(toBinary(ch))
	}
	
	return buf.String() 
}

func toBinary(ch rune) string {
	table := getEncodingTable()
	res, ok := table[ch]
	if !ok {
		// panic(fmt.Sprintf("unknown character: %d", int(ch - '0')))
		return "" 
	}
	return res
}

func getEncodingTable() table.EncodingTable {
	return table.EncodingTable{
		' ': "11",             'e': "101",
		't': "1001",           'o': "10001",
		'n': "10000",          'a': "011",
		's': "0101",           'i': "01001",
		'r': "01000",          'h': "0011",
		'd': "00101",          'l': "001001",
		'!': "001000",         'u': "00011",
		'c': "000101",         'f': "000100",
		'm': "000011",         'p': "0000101",
		'g': "0000100",        'w': "0000011",
		'b': "0000010",        'y': "0000001",
		'v': "00000001",       'j': "000000001",
		'k': "0000000001",     'x': "00000000001",
		'q': "000000000001",   'z': "000000000000",
	}
}

// prepares text (s) to be fit for encode:
// changes UPPER case letters to !+lower case letters
// e.g.: Hello Rouse -> !hello !rouse
func prepareText(s string) string {
	var buf strings.Builder

	for _, ch := range s {
		if unicode.IsUpper(ch) {
			buf.WriteRune('!')
			buf.WriteRune(unicode.ToLower(ch))	
			continue
		}
		buf.WriteRune(ch)
	}
	return buf.String() 
}

// opposite to prepareText, exports !m -> M
func exportText(s string) string {
	var buf strings.Builder
	
	var nextChIsCapital bool 

	for _, ch := range s {
		if nextChIsCapital {
			buf.WriteRune(unicode.ToUpper(ch))
			nextChIsCapital = false
			continue
		}
		if ch == '!' {
			nextChIsCapital = true
			continue
		}
		buf.WriteRune(ch)
	}

	return buf.String()
}