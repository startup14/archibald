package cmd

import (
	"archibald/lib/compression"
	"archibald/lib/compression/vlc"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
)

var unpackCmd = &cobra.Command{
	Use: "unpack",
	Short: "Unpack file",
	Run: unpack,
}

// todo: get original ext from file
const UNPACKED_EXT = ".txt"

func unpack(cmd *cobra.Command, args []string) {

	if len(args) < 1 || args[0] == "" {
		handleErr(ErrEmptyPath)
	}

	var decoder compression.Decoder
	method := cmd.Flag("method").Value.String() 
	switch method {
	case "vlc":
		decoder = vlc.New()
	default:
		cmd.PrintErrf("Unknown compression method: %s\n", method)
	}
	filePath := args[0]

	f, err := os.Open(filePath)
	if err != nil {
		handleErr(err)
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		handleErr(err)
	}

	unpacked := decoder.Decode(data)

	err = os.WriteFile(unpackedFileNameFrom(filePath), []byte(unpacked), 0644)
	if err != nil {
		handleErr(err)
	}
}

// todo: refactor: move up and use ext arg
func unpackedFileNameFrom(path string) string {
	fileName := filepath.Base(path)
	ext := filepath.Ext(fileName)
	baseName := strings.TrimSuffix(fileName, ext)
	
	return baseName + UNPACKED_EXT
}

func init() {
	rootCmd.AddCommand(unpackCmd)
	unpackCmd.Flags().StringP("method", "m", "", "decompression method: vlc")
	if err := unpackCmd.MarkFlagRequired("method"); err != nil {
		panic(err)
	} 
}