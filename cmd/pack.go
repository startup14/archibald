package cmd

import (
	"archibald/lib/compression"
	"archibald/lib/compression/vlc"
	"errors"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
)

var packCmd = &cobra.Command{
	Use: "pack",
	Short: "Pack file",
	Run: pack,
}

const VLC_EXT = ".vlc"

var ErrEmptyPath = errors.New("FIle to pack not provided. Usage: pack vlc <filename>")

func pack(cmd *cobra.Command, args []string) {
	if len(args) < 1 || args[0] == "" {
		handleErr(ErrEmptyPath)
	}

	var encoder compression.Encoder

	method := cmd.Flag("method").Value.String()
	switch method {
	case "vlc":
		encoder = vlc.New()
	default:
		cmd.PrintErrf("Unknown compression method: %s\n", method)
	}

	filePath := args[0]

	f, err := os.Open(filePath)
	if err != nil {
		handleErr(err)
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		handleErr(err)
	}

	packed := encoder.Encode(string(data))

	err = os.WriteFile(outputFileNameFrom(filePath), packed, 0644)
	if err != nil {
		handleErr(err)
	}
}

func outputFileNameFrom(path string) string {
	fileName := filepath.Base(path)
	ext := filepath.Ext(fileName)
	baseName := strings.TrimSuffix(fileName, ext)

	return baseName + VLC_EXT
}

func init() {
	rootCmd.AddCommand(packCmd)
	packCmd.Flags().StringP("method", "m", "", "compression method: vlc")
	if err := packCmd.MarkFlagRequired("method"); err != nil {
		panic(err)
	} 
}